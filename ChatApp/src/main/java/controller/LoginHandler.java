package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.model.Person;
import domain.service.Service;

public class LoginHandler extends RequestHandler
{

	public LoginHandler(Service service)
	{
		super(service);
	}

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
	{
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		Person person;

		person = service.getUserIfAuthenticated(name, password);
		HttpSession session = request.getSession();
		if (person != null)
		{
			session.setAttribute("person", person);
		}
		else
		{
			String error = "No vaild userid/password";
			session.setAttribute("error", error);
		}
		return "index.jsp";

	}
}
