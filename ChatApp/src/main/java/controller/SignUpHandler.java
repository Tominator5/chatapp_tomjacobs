package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.model.Person;
import domain.service.Service;

public class SignUpHandler extends RequestHandler
{
	public SignUpHandler(Service service)
	{
		super(service);
	}

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
	{
		String name = request.getParameter("newName");
		String password = request.getParameter("newPassword");
		Person person = new Person();
		person.setName(name);
		person.setPassword(password);
		service.add(person);
		return "index.jsp";
	}

}
