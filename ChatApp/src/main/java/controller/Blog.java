package controller;

import java.io.Console;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/blog")
public class Blog {

	private static final Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());

	@OnOpen
	public void onOpen(Session session)
	{
		sessions.add(session);
	}
	
	@OnMessage
	public void onMessage(String message, Session session)
	{
		sendMessage(message);
	}
	
	private void sendMessage(String message) {
		for (Session session : sessions)
		{
			try
			{
				session.getBasicRemote().sendText(message);
				System.out.println(session.getId());
			} catch (IOException ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	@OnClose
	public void onClose()
	{
		System.out.println("close (serverendpoint)");
	}
}
