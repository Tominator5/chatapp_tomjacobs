package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.service.Service;


public abstract class RequestHandler
{
	protected boolean redirect = false;
	protected Service service;

	public RequestHandler(Service service)
	{
		this.service = service;
	}
	
	public RequestHandler()
	{
		
	}

	public final void handle(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException
	{
		String destination = "index.jsp";

		destination = handleRequest(request, response);

		if (isRedirect())
		{
			response.sendRedirect(destination);
		}
		else
		{
			RequestDispatcher view = request.getRequestDispatcher(destination);
			view.forward(request, response);
		}

	}

	public void setRedirect(boolean redirect)
	{
		this.redirect = redirect;
	}

	public boolean isRedirect()
	{
		return this.redirect;
	}

	public abstract String handleRequest(HttpServletRequest request, HttpServletResponse response);
}
