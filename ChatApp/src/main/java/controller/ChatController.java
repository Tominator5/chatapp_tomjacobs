package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import domain.model.Message;
import domain.model.Person;
import domain.service.Service;


@WebServlet("/ChatController")
public class ChatController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Service service = new Service();

    public ChatController() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Person user = (Person)session.getAttribute("person");
		String name = (String) request.getParameter("friendId");
		Person friend = service.get(name);
		if (user != null && friend != null)
		{
			List<Message> chat = service.getMessages(user, friend);
			if (chat != null)
			{
				String chatJson = toJson(chat);
				response.setContentType("text/plain");
				PrintWriter out = response.getWriter();
				out.print(chatJson);
				out.flush();
				out.close();
			}
			
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Person user = (Person)session.getAttribute("person");
		String name = (String) request.getParameter("friendId");
		Person friend = service.get(name);
		Message message = new Message(user.getName(), request.getParameter("message"));
		service.addMessage(user, friend, message);
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		out.print(user.getName() + ": " + message.getBericht());
		out.flush();
		out.close();
	}
	
	private String toJson(List<Message> list) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String result = mapper.writeValueAsString(list);
		return result;
	}
}