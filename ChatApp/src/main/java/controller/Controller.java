package controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import domain.model.Person;
import domain.service.Service;


@WebServlet("/Controller")
public class Controller extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	private Service service = new Service();
	private HandlerFactory factory = new HandlerFactory(service);
	
	public Controller()
	{
		super();
	}

	@Override
	public void init() throws ServletException
	{
		super.init();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		requestHandler(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		requestHandler(request, response);
	}

	protected void requestHandler(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String action = (request.getParameter("action") != null ? request.getParameter("action") : "home");
		if (action.equals("updateStatus"))
		{
			updateStatus(request, response);
		}
		else if (action.equals("addFriend"))
		{
			addFriend(request, response);
		}
		else if (action.equals("getFriends"))
		{
			getFriends(request, response);
		}
		else if (action.equals("angular"))
		{
			proccesAngularRequest(request, response);
		}
		else
		{
			RequestHandler handler = factory.createHandler(action);
			handler.handle(request, response);
		}
		
	}

	private void proccesAngularRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<Person> persons = service.getAll();
		String users = toJson(persons);
		// setting the response type to json
		response.setContentType("application/json");
		// setting the CORS request, CrossOriginResourceSharing
		// so that this url/response is accessible in another domain (angular application for example) 
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.getWriter().write(users);
	}

	private void getFriends(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		Person person = (Person) session.getAttribute("person");
		List<Person> friends = service.getFriendsFor(person.getName());
		
		String friendsJson = toJson(friends);
		response.getWriter().write(friendsJson);
	}

	private void addFriend(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Person person = (Person) session.getAttribute("person");
		Person friend = service.get(request.getParameter("friendName"));
		person.addFriend(friend);
		friend.addFriend(person);
		service.update(person);
		service.update(friend);
	}

	private String toJson(List<Person> friends) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String result = mapper.writeValueAsString(friends);
		return result;
	}

	private void updateStatus(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String status = request.getParameter("status");
		
		HttpSession session = request.getSession();
		Person person = (Person) session.getAttribute("person");
		String name = person.getName();
		service.get(name).setStatus(status);
		
		String statusXML = this.toXML(status);
		
		response.setContentType("text/xml");
		response.getWriter().write(statusXML);
	}  	
		
	public String toXML(String status) {
		StringBuffer xmlDoc = new StringBuffer();
			
		xmlDoc.append("<status>\n");
		xmlDoc.append(status);
		xmlDoc.append("</status>\n");
			
		return xmlDoc.toString();
	}
}