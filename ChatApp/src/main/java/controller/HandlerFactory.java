package controller;

import java.util.HashMap;
import java.util.Map;

import domain.service.Service;

public class HandlerFactory
{
	private Map<String, RequestHandler> map;
	private Service service;

	public HandlerFactory(Service service)
	{
		this.service = service;
		
		map = new HashMap<String, RequestHandler>();

		map.put("home", new HomePageHandler());
		map.put("login", new LoginHandler(service));
		map.put("logout", new LogoutHandler());
		map.put("signUp", new SignUpHandler(service));
		
	}
	
	public RequestHandler createHandler(String action)
	{
		return map.get(action);
	}

}
