package domain.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import domain.model.Person;

public class PersonRepositoryInMemory implements PersonRepository {
	private Map<Integer, Person> persons = new HashMap<Integer, Person>();
	private static int counter = 10;
	
	public PersonRepositoryInMemory () {
		this.add(new Person(1, "Stef", "t", "offline"));
		this.add(new Person(2, "Sam", "t"));
		this.add(new Person(3, "Marie", "t", "altijd online!"));
		this.add(new Person(4, "Arno", "t", "Vandaag een beetje gek"));
		this.add(new Person(5, "Dries", "t"));
		this.add(new Person(6, "Thomas", "t", "salty"));
		this.add(new Person(7, "Sarah", "t"));
		this.add(new Person(8, "Tom", "t"));
	}
	
	public Person get(String name){
		if(name == null){
			throw new DatabaseException("No name given");
		}
		List<Person> list = new ArrayList<Person>(persons.values());
		for (Person p: list)
		{
			if(p.getName().equals(name))
			{
				return persons.get(p.getId());
			}
		}
		return null;
	}
	
	public List<Person> getAll(){
		return new ArrayList<Person>(persons.values());	
	}

	public void add(Person person){
		if(person == null){
			throw new DatabaseException("No person given");
		}
		if (persons.containsKey(person.getName())) {
			throw new DatabaseException("User already exists");
		}
		person.setId(counter);
		counter++;
		persons.put(person.getId(), person);
	}
	
	public void delete(String name){
		if(name == null){
			throw new DatabaseException("No name given");
		}
		persons.remove(name);
	}

	public void update(Person person)
	{
		if(person == null){
			throw new DatabaseException("No person given");
		}
		persons.put(person.getId(), person);
		
	}
}
