package domain.db;

import java.util.ArrayList;
import java.util.List;

import domain.model.Chat;
import domain.model.Person;

public class ChatRepositoryInMemory 
{
	private List<Chat> chats;
	
	public ChatRepositoryInMemory()
	{
		chats = new ArrayList<Chat>();
	}
	
	public void addChat(Chat chat)
	{
		chats.add(chat);
	}
	
	public Chat getChat(Person p1, Person p2)
	{
		Chat result = null;
		for (Chat chat : chats)
		{
			if (chat.getPerson1().equals(p1))
			{
				if (chat.getPerson2().equals(p2))
				{
					result =  chat;
					System.out.println("chat found: A " + p1.getName() + p2.getName());
				}
			}
			else if (chat.getPerson1().equals(p2))
			{
				if (chat.getPerson2().equals(p1))
				{
					result =  chat;
					System.out.println("chat found: B " + p2.getName() + p1.getName());
				}
			}
		}
		if (result == null)
		{
			Chat chat = new Chat(p1, p2);
			chats.add(chat);
			result = chat;
			System.out.println("NOT " + p1.getName() + p2.getName());
		}

		return result;
	}
	
}