package domain.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import domain.db.ChatRepositoryInMemory;
import domain.db.PersonRepository;
import domain.db.PersonRepositoryInMemory;
import domain.model.Chat;
import domain.model.Message;
import domain.model.Person;

public class Service
{
	private PersonRepository persons;
	private ChatRepositoryInMemory chats;
	
	public Service()
	{
		persons = new PersonRepositoryInMemory();
		chats = new ChatRepositoryInMemory();
	}
	
	public Person get(String name)
	{
		return persons.get(name);
	}
	
	public List<Person> getAll()
	{
		return persons.getAll();
	}

	public void add(Person person)
	{
		persons.add(person);
	}

	public void update(Person person)
	{
		persons.update(person);
	}

	public void delete(String name)
	{
		persons.delete(name);
	}
	
	public List<Person> getFriendsFor(String name)
	{
		Person person = persons.get(name);
		return person.getFriends();
	}
	
	public Person getUserIfAuthenticated(String name, String password)
	{
		Person person = this.get(name) != null ? this.get(name) : null;
		
		if (person == null || !person.isPasswordCorrect(password))
		{
			person = null;
		}
		
		return person;
	}
	
	public void addMessage(Person from, Person to, Message message)
	{
		Chat chat = chats.getChat(from, to);
		if (chat == null)
		{
			chat = new Chat(from, to);
			chats.addChat(chat);
		}
		chat.addMessage(message);
	}
	
	public List<Message> getMessages(Person p1, Person p2)
	{
		return chats.getChat(p1, p2).getMessages();
	}
	
}