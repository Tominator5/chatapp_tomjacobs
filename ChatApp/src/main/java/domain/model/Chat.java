package domain.model;

import java.util.ArrayList;
import java.util.List;

public class Chat 
{
	private Person person1;
	private Person person2;
	private List<Message> messages;
	
	public Chat(Person p1, Person p2)
	{
		this.person1 = p1;
		this.person2 = p2;
		this.messages = new ArrayList<Message>();
	}

	public Person getPerson1() {
		return person1;
	}

	public void setPerson1(Person person1) {
		this.person1 = person1;
	}

	public Person getPerson2() {
		return person2;
	}

	public void setPerson2(Person person2) {
		this.person2 = person2;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	public void addMessage(Message message)
	{
		messages.add(message);
	}

}