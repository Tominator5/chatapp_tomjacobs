package domain.model;

public class Message {

	private String naam, bericht;
	
	public Message(String naam, String bericht)
	{
		setNaam(naam);
		setBericht(bericht);
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getBericht() {
		return bericht;
	}

	public void setBericht(String bericht) {
		this.bericht = bericht;
	}
	
	
}
