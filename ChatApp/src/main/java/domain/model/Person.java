package domain.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Person
{
	private int id;
	
	private String name, status;
	
	private String password;
	@JsonIgnore
	private List<Person> friends;
	
	public Person()
	{
		
	}
	
	public Person(int id, String name, String password, String status)
	{
		setId(id);
		setName(name);
		setStatus(status);
		setPassword(password);
		friends = new ArrayList<Person>();
	}
	
	public Person(int id, String name, String password)
	{
		this(id, name, password, "online");
	}

	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}
	
	public void addFriend(Person person)
	{
		if (!friends.contains(person))
		{
			friends.add(person);
		}
	}
	
	public List<Person> getFriends()
	{
		return this.friends;
	}

	public boolean isPasswordCorrect(String password)
	{
		return this.password.equals(password);
	}
	
	@Override
	public boolean equals(Object object)
	{
		Person person = (Person)object;
		return this.getId() == person.getId();
	}
	
	
}