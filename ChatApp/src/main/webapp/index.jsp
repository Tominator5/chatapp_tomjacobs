<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Chat App</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/styles.css" />

</head>
<body onload="getFriends()">
	<div id="container">
		<div id="header">
			<h1>Chat Application</h1>
		</div>
		<div id="wrapper">
			<div id="content">
				<c:choose>
					<c:when test="${empty person}">
						<p>Login: </p>
						<p>${error}</p>
						<form method="post" action="Controller?action=login" novalidate="novalidate">
							<p>
								<label for="name">Your Name:</label> 
								<input type="text" id="name" name="name" required value="<c:out value=''/>">
							</p>
							<p>
								<label for="password">Your password:</label> 
								<input type="password" id="password" name="password" required value="<c:out value=''/>">
							</p>
							<p>
								<input type="hidden" name="action" value="login"> 
								<input type="submit" value="Login" id="login">
							</p>
						</form>
					</c:when>
					<c:otherwise>
						<h2>Ingelogd als: ${person.name}</h2>
						<h2>My friends:</h2>
						<ul id="friends">
							
						</ul>
						<c:if test="${not empty person}">
							<p>
								<label for="friendName">Naam: </label> 
								<input type="text" name="friendName" id="friendName">
							</p>
							<p>
								<input type="button" value="add" id="addFriendButton">
							</p>
						</c:if>
					</c:otherwise>
				</c:choose>
				
			</div>
			
		</div>
		<div id="navigation">
		<c:choose>
			<c:when test="${not empty person}">
			<p></p>
			<ul>
				<li>Status: </li>
				<li><div id="currentStatus"></div></li>
				<li>
					
				<p>
					<input type="text" name="status" id="status" value="<c:out value=''/>">
				</p>
				<p>
					<input type="button" value="update" id="updatebutton">
				</p>
					
				</li>
			</ul>
			</c:when>
			<c:otherwise>
			
			<ul>
				<li>New? Sign up now!</li>
				<li>
					<form method="post" action="Controller?action=signUp">
						<p>
								<label for="newName">Your Name:</label> 
								<input type="text" id="newName" name="newName" required value="<c:out value=''/>">
							</p>
							<p>
								<label for="newPassword">Your password:</label> 
								<input type="text" id="newPassword" name="newPassword" required value="<c:out value=''/>">
							</p>
							<p>
								<input type="submit" value="Sign up" id="signUp">
							</p>
					</form>
				</li>
			</ul>
			</c:otherwise>
			</c:choose>
		</div>
		<div id="extra">
			<p><a href="blog.jsp">Blog</a><p>
		</div>
		<div id="footer">
		<c:if test="${not empty person}">
			<form method="post" action="Controller?action=logout">
				<p>
					<input type="submit" value="Logout">
				</p>
			</form>
		</c:if>
		</div>
	</div>
	<div class="msg_box">
		<div class="msg_head">
			<div class="chatPartner"></div>
			<div class="close">x</div>
		</div>
		<div class="msg_wrap">
			<div class="msg_body">
				<div class="msg_push"></div>
			</div>
			<div class="msg_footer"><textarea class="msg_input" rows="4"></textarea></div>
		</div>
	</div>


	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="chatapp.js"></script>
</body>

</html>
