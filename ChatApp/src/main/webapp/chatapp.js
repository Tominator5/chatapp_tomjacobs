var updatebutton = document.getElementById('updatebutton');
updatebutton.onclick = updateStatus;

var xHRObject = new XMLHttpRequest();

function updateStatus() {
	var status = document.getElementById("status").value;
	// encodeURIComponent om UTF-8 te gebruiken en speciale karakters om te zetten naar code
	var information = "status=" + encodeURI(status);
	xHRObject.open("POST", "Controller?action=updateStatus", true);
	xHRObject.onreadystatechange = getStatus;
	
	// belangrijk dat dit gezet wordt anders kan de servlet de informatie niet interpreteren!!!
	// protocol header information
	xHRObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xHRObject.send(information);
}

function getStatus() {
	if (xHRObject.readyState == 4) {
		if (xHRObject.status == 200) {
			var serverResponse = xHRObject.responseXML;
			var statusXML = serverResponse.getElementsByTagName("status");
			
			var statusDiv = document.getElementById("currentStatus");
			var statusParagraph = statusDiv.childNodes[0];
			
			if (statusParagraph == null) {
				statusParagraph = document.createElement('p');
				statusParagraph.id = "statusText";
				var statusText = document.createTextNode(statusXML[0].textContent);
				statusParagraph.appendChild(statusText);
				statusDiv.appendChild(statusParagraph);
			}
			else {
				var statusText = document.createTextNode(statusXML[0].textContent);
				statusParagraph.removeChild(statusParagraph.childNodes[0]);
				statusParagraph.appendChild(statusText);
			}	
		}
	}
}

var addFriendButton = document.getElementById('addFriendButton');
addFriendButton.onclick = addFriend;

var addFriendObject = new XMLHttpRequest()

function addFriend() {
	var friend = document.getElementById("friendName").value;
	var information = "friendName=" + encodeURI(friend);
	addFriendObject.open("POST", "Controller?action=addFriend", true);
	
	addFriendObject.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	addFriendObject.send(information);
}

var friendsGenerator = new XMLHttpRequest();

function getFriends() {
	friendsGenerator = new XMLHttpRequest();
	friendsGenerator.open("GET", "Controller?action=getFriends", true);
	friendsGenerator.onreadystatechange = getFriendsList;
	friendsGenerator.send(null);
}

function getFriendsList () {
	if (friendsGenerator.readyState == 4) {
		if (friendsGenerator.status == 200) {
			var friends = JSON.parse(friendsGenerator.responseText);
			var friendsUL = document.getElementById("friends");
			
			var oldLength = friendsUL.childNodes.length;
			for (var i = 0; i < oldLength; i++)
			{
				friendsUL.removeChild(friendsUL.childNodes[0]);
			}
			
			for (var j = 0; j< friends.length; j++)
			{
				var friendsLI = document.createElement('li');
				friendsLI.setAttribute('class', 'friend');
				friendsLI.setAttribute('id', friends[j].name);
				var friendsText = document.createTextNode(friends[j].name + " " + friends[j].status);
				friendsLI.appendChild(friendsText);
				friendsUL.appendChild(friendsLI);
			}
			setTimeout("getFriends()", 3000);
		}
	}
}

function getMessages() {
	$('.message').remove();
	$.get("ChatController", {friendId: $('.chatPartner').text()}, function(data) {
		var messages = JSON.parse(data)
		console.log('lijn 97' +messages);
		$.each(messages, function(i, item) {
			console.log(item);
			$('<div class="message"/>').text(item.naam + ': ' + item.bericht).insertBefore('.msg_push');
		})
		$('.msg_body').scrollTop($('.msg_body')[0].scrollHeight);
	})
}



$(document).ready(function(){
	
		
	
	$("body").on("click", ".friend", function(){
		var friendId = $(this).attr('id');
		$('.chatPartner').empty();
		$('.chatPartner').append(friendId);
		$('.msg_wrap').show();
		$('.msg_box').show();
		setInterval(getMessages, 4000); //4 sec,zodat log nog leesbaar is, normaal zou sneller mogen
	});
	
	
	
	$('textarea').keypress(
		    function(e){
		        if (e.keyCode == 13) {
		        	
		            e.preventDefault();
		            var msg = $(this).val();
					if(msg != '')
					{
						$.post("ChatController", {friendId: $('.chatPartner').text(),message: msg}, function(data) {
							$('<div class="message"/>').text(data).insertBefore('.msg_push');
						})
					}
		        }
		    });

	$('.chat_head').click(function(){
		$('.chat_body').slideToggle('slow');
	});
	$('.msg_head').click(function(){
		$('.msg_wrap').slideToggle('slow');
	});
	
	$('.close').click(function(){
		$('.msg_box').hide();
	});
	
	
	
});