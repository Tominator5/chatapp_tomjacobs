<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Chat App</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/styles.css" />

</head>
<body onload="openSocket()">
	<div id="container">
		<div id="header">
			<h1>Chat Application</h1>
		</div>
		<div id="wrapper">
			
				
		<div id="extra">
			<h2>Blogs</h2>
			<h3>Wat is je favoriete Harry Potter film?</h3>
			<div class="panel1">
				<p><input type="text" id="blog1" value="<c:out value=''/>"></p>
				<p><button type="button" id="buttonBlog1">Send</button></p>
				<div id="messagesBlog1"></div>
			</div>
			<p class="flip1">Show/Hide Panel</p>
				
			<h3>Wat is je favoriete spreuk in Harry Potter?</h3>
			<div class="panel2">
				<p><input type="text" id="blog2" value="<c:out value=''/>"></p>
				<p><button type="button" id="buttonBlog2">Send</button></p>
				<div id="messagesBlog2"></div>
			</div>
			<p class="flip2">Show/Hide Panel</p>
				
			<h3>Wie is de beste leerkracht Verweer tegen de Zwarte Kunsten in Harry Potter?</h3>
			<div class="panel3">
				<p><input type="text" id="blog3" value="<c:out value=''/>"></p>
				<p><button type="button" id="buttonBlog3">Send</button></p>
				<div id="messagesBlog3"></div>
			</div>
			<p class="flip3">Show/Hide Panel</p>
		</div>
		<div id="footer">
			<p><a href="index.jsp">Home</a><p>
		</div>
	</div>
	</div>
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="blog.js"></script>
	
</body>

</html>
