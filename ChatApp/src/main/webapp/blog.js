var webSocket;
var messagesBlog1 = document.getElementById("messagesBlog1");
var messagesBlog2 = document.getElementById("messagesBlog2");
var messagesBlog3 = document.getElementById("messagesBlog3");
var activeBlog = 0;

function openSocket()
{
	webSocket = new WebSocket("ws://localhost:8080/Chat_App/blog");
	console.log("open");
	webSocket.onopen = function(event){	};
	webSocket.onmessage = function(event) {
		console.log(event.data);
		if (activeBlog == 1)
		{
			writeResponseBlog1(event.data);
		}
		else if (activeBlog == 2)
		{
			writeResponseBlog2(event.data);
		}
		else if (activeBlog == 3)
		{
			writeResponseBlog3(event.data);
		}
		
	};
	webSocket.onclose = function(event) { console.log('close')};
}

function writeResponseBlog1(text)
{
	var paragraph = document.createElement('p');
	var paragraphText = document.createTextNode(text);
	paragraph.appendChild(paragraphText);
	messagesBlog1.appendChild(paragraph);
}

function writeResponseBlog2(text)
{
	var paragraph = document.createElement('p');
	var paragraphText = document.createTextNode(text);
	paragraph.appendChild(paragraphText);
	messagesBlog2.appendChild(paragraph);
}

function writeResponseBlog3(text)
{
	var paragraph = document.createElement('p');
	var paragraphText = document.createTextNode(text);
	paragraph.appendChild(paragraphText);
	messagesBlog3.appendChild(paragraph);
}

var buttonBlog1 = document.getElementById('buttonBlog1');
buttonBlog1.onclick = editBlog1;

function editBlog1()
{
	activeBlog = 1;
	var text = document.getElementById("blog1").value;
	console.log('b1 ' + text);
	webSocket.send(text);

}

var buttonBlog2 = document.getElementById('buttonBlog2');
buttonBlog2.onclick = editBlog2;

function editBlog2()
{
	activeBlog = 2;
	var text = document.getElementById("blog2").value;
	console.log('b2 ' +text);
	webSocket.send(text);

}

var buttonBlog3 = document.getElementById('buttonBlog3');
buttonBlog3.onclick = editBlog3;

function editBlog3()
{
	activeBlog = 3;
	var text = document.getElementById("blog3").value;
	console.log('b3 ' +text);
	webSocket.send(text);

}


//Jquery
$(document).ready(function(){
	$(".flip1").click(function(){
		$(".panel1").slideToggle("slow");
		});
});

$(document).ready(function(){
	$(".flip2").click(function(){
		$(".panel2").slideToggle("slow");
		});
});

$(document).ready(function(){
	$(".flip3").click(function(){
		$(".panel3").slideToggle("slow");
		});
});
